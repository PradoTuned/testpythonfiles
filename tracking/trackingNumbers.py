import csv

# Constants for storing the file locations and tracking prefix
raw_tracking_file = "/Users/hector/Projects/Active/test_python_files/TestPythonFiles/tracking/testTracking.csv"
raw_tracking_file1 = "/Users/hector/Projects/Active/test_python_files/TestPythonFiles/tracking/testTracking_updated.csv"
tracking_prefix = "http://parcelsapp.com/en/tracking/"

# open up the file in read mode and store the csv as an object
with open(raw_tracking_file, encoding="utf-8") as rawfile:
    field_names = []                                # variable to hold the "header" for the new file (e.g. tracking,full_tracking)
    tempFile = csv.DictReader(rawfile)              # read the file into a csv DictReader Object for iteration of each column item
    field_names.append(tempFile.fieldnames[0])      # add the first header found into the field_names
    field_names.append("full_tracking")             # add the "full_tracking" header
    updatedFile = []                                # variable to store the tracking and full_tracking columns
    
    # iterate through the dictionat (tempFile) and start creating the full_tracking and append to updatedFile list
    for line in tempFile:
        line[field_names[1]] = tracking_prefix + line["tracking"]
        print(line)
        updatedFile.append(line)

    # open up a DictWriter object to now write the field_names
    # and store the updatedFile list as rows corresponding to each field_name column
    with open(raw_tracking_file1, "w", newline="") as updateFile:
        writer = csv.DictWriter(updateFile, fieldnames=field_names)

        writer.writeheader()
        writer.writerows(updatedFile)
