from PIL import Image, ImageDraw
import face_recognition
import cv2
import numpy as np

# video_capture = cv2.VideoCapture(0)

obama_image = face_recognition.load_image_file(
    "/Users/hector/Projects/Active/test_python_files/TestPobama.jpg")
obama_face_encoding = face_recognition.face_encodings(obama_image)[0]

hector_image = face_recognition.load_image_file(
    "/Users/hector/Projects/Active/test_python_files/hector.jpeg")
#hector_face_encoding = face_recognition.face_encodings(hector_image)[0]

leeanne_image = face_recognition.load_image_file(
    "/Users/hector/Projects/Active/test_python_files/leeanne.jpeg")
#leeanne_face_encoding = face_recognition.face_encodings(leeanne_image)[0]


known_face_encodings = [
    obama_face_encoding,
    # hector_face_encoding,
    # leeanne_face_encoding
]

known_face_names = [
    "Barack Obama",
    # "Hector P",
    # "Leeanne N"

]

face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

face_locations = face_recognition.face_locations(obama_image)
face_encodings = face_recognition.face_encodings(
    obama_image, face_locations)

pil_image = Image.fromarray(obama_image)

draw = ImageDraw.Draw(pil_image)
for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
    top += 4
    right += 4
    bottom += 4
    left += 4

    draw.rectangle(((left, top), (right, bottom)), outline=(0, 0, 255))

    text_width, text_height = draw.textsize("Obama")
    draw.rectangle(((left, bottom-text_height - 10), (right, bottom)),
                   fill=(255, 0, 0), outline=(0, 0, 0))
    draw.text((left + 6, bottom - text_height - 5),
              "Obama", fill=(255, 255, 255))

del draw

pil_image.show()

'''
while True:
    ret, frame = video_capture.read()

    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

    rgb_small_frame = small_frame[:, :, ::-1]

    if process_this_frame:
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(
            rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            matches = face_recognition.compare_faces(
                known_face_encodings, face_encoding)
            name = "Unkown"

            if True in matches:
                first_match_index = matches.index(True)
                name = known_face_names[first_match_index]

        process_this_frame = not process_this_frame

        for (top, right, bottom, left), name in zip(face_locations, face_names):
            top += 4
            right += 4
            bottom += 4
            left += 4

            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            cv2.rectangle(frame, (left, bottom - 35),
                          (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6),
                        font, 1.0, (255, 255, 255), 1)

        cv2.imshow('Video', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

video_capture.release()
cv2.destroyAllWindows()
'''
